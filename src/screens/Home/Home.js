import React from "react";
import { StyleSheet, View, Text, Button } from "react-native";
import { useNavFunc } from "../../navigation/useNavFunc";
import { PlayListItem, YoutubePlayer } from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { YoutubeAction } from "../../redux/actions";
import FirebaseService from "../../services/FirebaseService";

const Home = () => {
  const { navigation } = useNavFunc();
  const onPressSearch = () => {
    navigation.navigate("SearchVideo");
  };
  const { listMusic, playing } = useSelector((state) => state.youtube);
  const dispatch = useDispatch();

  const onEnd = () => {
    dispatch({
      type: YoutubeAction.NEXT_SONG,
    });
  };

  return (
    <View style={styles.container}>
      {Boolean(playing) && (
        <YoutubePlayer
          onEnd={onEnd}
          videoId={playing.videoId}
          name={playing.name}
          author={playing.author}
          totalView={3000000}
        />
      )}
      <Text style={styles.playListTitle}>Playlist</Text>
      {listMusic.map((item, i) => {
        return (
          <PlayListItem
            key={`item${i}`}
            thumb={item.thumb}
            name={item.name}
            author={item.author}
            onRemove={() => {
              FirebaseService.deleteSong(item.keyVideo);
              dispatch({
                type: YoutubeAction.REMOVE_ITEM,
                data: item,
              });
            }}
            onPress={() => {
              //play bai hat o local
              FirebaseService.deleteSong(item.keyVideo);
              dispatch({
                type: YoutubeAction.PLAY_SONG,
                data: item,
              });
              //Xoa bai hat khoi firebase
            }}
          />
        );
      })}
      <View style={styles.btnAdd}>
        <Button title="ADD" onPress={onPressSearch} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0e0b1e",
  },
  playListTitle: {
    fontSize: 22,
    fontWeight: "bold",
    marginLeft: 24,
    marginTop: 30,
    color: "white",
  },
  btnAdd: {
    paddingHorizontal: 50,
    paddingVertical: 20,
  },
});

Home.routeInfo = {
  title: "Home Screens",
  path: "/home",
};

export default Home;
