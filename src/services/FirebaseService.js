import { initializeApp } from "firebase/app";
import {
  getDatabase,
  ref,
  set,
  remove,
  push,
  onChildAdded,
} from "firebase/database";
import Store from "../redux/Store";
import { YoutubeAction } from "../redux/actions";
import YoutubeService from "./YoutubeService";

const FirebaseService = {
  apiKey: "AIzaSyA3qs_CdQEsUWGN3NSXiKb-pYP1QksdNMg",
  authDomain: "play-d7d6a.firebaseapp.com",
  databaseURL: "https://play-d7d6a-default-rtdb.firebaseio.com",
  projectId: "play-d7d6a",
  storageBucket: "play-d7d6a.appspot.com",
  messagingSenderId: "1003743517504",
  appId: "1:1003743517504:web:b644789b12f3924e5c78a5",
  measurementId: "G-YLLXB3026M",
};
initializeApp(FirebaseService);

class FirebaseData {
  constructor() {
    this.onNewSongAdded();
  }

  addNewSong = (data) => {
    const db = getDatabase();
    const songListRef = ref(db, "list");
    const newSongRef = push(songListRef);
    set(newSongRef, {
      ...data,
    });
    // thêm bài hát lên server
  };

  onNewSongAdded = () => {
    const db = getDatabase();
    const songListRef = ref(db, "list");
    onChildAdded(songListRef, (data) => {
      console.log("new song", data.val());
      console.log(data.ref._path.pieces_[1]);
      const keyVideo = data.ref._path.pieces_[1];
      // xử lý bài mới ở đây, đẩy vào list redux, cho play
      Store.dispatch({
        type: YoutubeAction.SELECT_ITEM,
        data: { ...data.val(), keyVideo },
      });
      YoutubeService.initPlayer();
    });
  };
  deleteSong = (keyVideo) => {
    const db = getDatabase();
    remove(ref(db, "list" + `/${keyVideo}`));
  };
}

export default new FirebaseData();
