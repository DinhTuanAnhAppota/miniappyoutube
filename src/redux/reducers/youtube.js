import { YoutubeAction } from "../actions";

const initialState = {
  playing: undefined,
  listMusic: [],
};
const youtube = (state = initialState, action) => {
  switch (action.type) {
    case YoutubeAction.SELECT_ITEM:
      let oldList = [...state.listMusic];
      oldList.push(action.data);
      return {
        ...state,
        listMusic: oldList,
      };
    case YoutubeAction.UPDATE_LIST:
      return {
        ...state,
        listMusic: action.data,
      };
    case YoutubeAction.NEXT_SONG:
      if (!state.listMusic.length) return state;
      let nextSong = state.listMusic[0];
      let newList = [...state.listMusic];
      newList = newList.slice(1);
      return {
        ...state,
        listMusic: newList,
        playing: nextSong,
      };
    case YoutubeAction.PLAY_SONG:
      let arr = state.listMusic.filter(
        (i) => i.videoId !== action.data.videoId
      );
      return {
        ...state,
        listMusic: arr,
        playing: action.data,
      };
    case YoutubeAction.REMOVE_ITEM:
      let arrRemoved = state.listMusic.filter(
        (i) => i.videoId !== action.data.videoId
      );
      return {
        ...state,
        listMusic: arrRemoved,
      };
    default:
      return state;
  }
};

export default youtube;
